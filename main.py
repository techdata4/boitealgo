import math


def calculate_number_of_cartons(nombre_de_boite, taille_des_boites, taille_des_cartons):
    # Gestion des cas de bord
    if nombre_de_boite <= 0:
        return "Erreur : Le nombre de boîtes doit être positif."
    if taille_des_boites <= 0 or taille_des_cartons <= 0:
        return "Erreur : Les tailles des boîtes et des cartons doivent être positives."
    if taille_des_boites > taille_des_cartons:
        return (
            "Erreur : La taille des boîtes ne peut être supérieure à celle des cartons."
        )

    nombre_de_boite_max_par_carton = taille_des_cartons // taille_des_boites
    print(
        f"Je peux ranger {nombre_de_boite_max_par_carton} boîtes dans un carton de taille {taille_des_cartons}"
    )
    nombre_de_carton = math.ceil(nombre_de_boite / nombre_de_boite_max_par_carton)

    return nombre_de_carton


# Exemples d'utilisation
print(calculate_number_of_cartons(10, 1, 5))  # Cas normal
print(calculate_number_of_cartons(0, 1, 5))  # Nombre de boîtes est zéro
print(calculate_number_of_cartons(10, 0, 5))  # Taille des boîtes est zéro
# Taille des boîtes est plus grande que celle des cartons
print(calculate_number_of_cartons(10, 6, 5))


# question 1.2 1 bis.
# Cartons tous la meme taille TC. Boites toutes la meme taille TB.
# Si j'ai X cartons, combien je peux ranger de boite ?
def calculate_number_box(nombre_cartons, taille_boite, taille_carton):
    # Vérifier si le nombre de cartons est valide
    if nombre_cartons <= 0:
        return "Erreur : Le nombre de cartons doit être positif et non nul."

    # Vérifier si la taille des boîtes est valide
    if taille_boite <= 0:
        return "Erreur : La taille des boîtes doit être positive et non nulle."

    # Vérifier si la taille des cartons est valide
    if taille_carton <= 0:
        return "Erreur : La taille des cartons doit être positive et non nulle."

    # Calculer le nombre de boîtes qui peuvent tenir dans un carton
    capacite_par_carton = taille_carton // taille_boite

    if capacite_par_carton == 0:
        return "Erreur : Les boîtes ne peuvent pas tenir dans les cartons."

    nombre_total_boites = capacite_par_carton * nombre_cartons

    return nombre_total_boites


print(calculate_number_box(5, 1, 10))  # Cas normal
print(calculate_number_box(0, 1, 10))  # Nombre de cartons est zéro
print(calculate_number_box(5, 0, 10))  # Taille des boîtes est zéro
print(calculate_number_box(5, 11, 10))
