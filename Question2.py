import random
from typing import List, Tuple, Dict


def estimer_max_boites(
    nb_boites: int,
    nb_cartons: int,
    taille_boite_min: int,
    taille_boite_max: int,
    taille_carton: int,
) -> float:
    # Qui a dit que les maths ne pouvaient pas être amusantes ?
    # Assurons-nous d'abord que nous ne comparons pas des pommes avec des oranges.
    if not all(
        isinstance(i, int)
        for i in (
            nb_boites,
            nb_cartons,
            taille_boite_min,
            taille_boite_max,
            taille_carton,
        )
    ):
        raise TypeError("Erreur : Les valeurs entrées doivent être de type int.")

    # Ici, nous faisons face à la réalité: une grande boîte ne rentrera jamais dans un petit carton.
    if taille_boite_min > taille_boite_max:
        raise ValueError(
            "Erreur : Taille minimale de la boîte supérieure à la taille maximale."
        )

    # La négativité n'est pas autorisée ici, seulement des tailles positives!
    if taille_boite_min <= 0 or taille_boite_max <= 0 or taille_carton <= 0:
        raise ValueError("Erreur : Les tailles doivent être supérieures à zéro.")

    # Il faut au moins une boîte et un carton pour que la fête commence !
    if nb_boites <= 0 or nb_cartons <= 0:
        raise ValueError(
            "Erreur : Le nombre de boîtes et de cartons doit être supérieur à zéro."
        )

    # Calculer la taille moyenne, parce que parfois la moyenne, c'est assez bien.
    taille_moyenne_boite = (taille_boite_min + taille_boite_max) / 2
    boites_par_carton = taille_carton // taille_moyenne_boite
    return min(boites_par_carton * nb_cartons, nb_boites)


def ranger_boites_dans_cartons(
    liste_boites: List[int], liste_cartons: List[int]
) -> Tuple[List[Dict[str, object]], List[int]]:
    """
    Organiser des boîtes dans des cartons, c'est un peu comme jouer à Tetris dans la vraie vie.
    Sauf qu'ici, aucune ligne ne disparaît quand on fait un bon coup.
    """
    sorted_boites = sorted(liste_boites)
    sorted_cartons = sorted(liste_cartons)
    cartons_organises = [
        {"espace_libre": taille, "boites_rangees": []} for taille in sorted_cartons
    ]

    # Essayons de ranger ces boîtes sans devoir appuyer sur 'Start Over'.
    for boite in sorted_boites:
        for carton in cartons_organises:
            if carton["espace_libre"] >= boite:
                carton["espace_libre"] -= boite
                carton["boites_rangees"].append(boite)
                break

    # Les boîtes restantes sont celles qui n'ont pas réussi à se faire des amis avec les cartons.
    boites_rangees = [
        item
        for sublist in [carton["boites_rangees"] for carton in cartons_organises]
        for item in sublist
    ]
    boites_non_rangees = [
        boite for boite in sorted_boites if boite not in boites_rangees
    ]

    return cartons_organises, boites_non_rangees


 # voyons ce cirque en action !
nb_boites = 10
taille_boite_min = 1
taille_boite_max = 3
nb_cartons = 12
taille_carton = 10
nombre_boites_estime = estimer_max_boites(
    nb_boites, nb_cartons, taille_boite_min, taille_boite_max, taille_carton
)


boites = [
    random.randint(taille_boite_min, taille_boite_max)
    for _ in range(nombre_boites_estime)
]
cartons = [taille_carton] * nb_cartons

# Action ! Rangeons ces boîtes et voyons le résultat.
cartons_organises, boites_non_rangees = ranger_boites_dans_cartons(boites, cartons)
print("Information sur le remplissage des cartons:", cartons_organises)
print("Boîtes non rangées:", boites_non_rangees)
